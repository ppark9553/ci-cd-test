const axios = require('axios');

const serverURL = 'https://kirinapi.web.qrft.io/';
const sampleQuery = `
query {
  Compustat {
    CoAfnd(gvkey: ["001003"]) {
      gvkey
      at
      Company {
        conm
        gsector
      }
    }
  }
}
`;

const getData = async () => {
  let params = { query: sampleQuery };
  let res = await axios.post(serverURL, { ...params });
  return res;
}


// async function main() {
//   let res = await getData();
//   console.log(res);
// }

// main();


module.exports = { getData };