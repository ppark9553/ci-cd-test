let chai = require('chai');
let chaiHttp = require('chai-http');
let should = chai.should();
let expect = chai.expect;

let { getData } = require('../req');

chai.use(chaiHttp);

const testServerURL = 'https://kirinapi.web.qrft.io/';

describe('Simple GraphQL Request', () => {
  it('should send simple GraphQL server quests and get return values', (done) => {

    const sampleQuery = `
    query {
      Compustat {
        CoAfnd(gvkey: ["001003"]) {
          gvkey
          at
          Company {
            conm
            gsector
          }
        }
      }
    }
    `;

    chai.request(testServerURL)
      .post('/')
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')
      .send({ query: sampleQuery })
      .end((err, res) => {
        res.should.have.status(200);
        done();
      });
  });
});

describe('res.js Functions', () => {
  it('getData()', (done) => {
    getData()
      .then((res) => {
        res.should.have.status(200);
        expect(res).to.have.property('data');
        expect(res.data.data).to.have.property('Compustat');
        expect(res.data.data.Compustat).to.have.property('CoAfnd');
        done();
      })
      .catch((err) => {
        console.log(err);
      });
  });
});